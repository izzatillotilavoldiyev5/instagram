package uz.pdp.instagram.exception;

public class MyCustomException extends RuntimeException{
    public MyCustomException() {
    }

    public MyCustomException(String message) {
        super(message);
    }
}
