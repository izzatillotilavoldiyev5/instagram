package uz.pdp.instagram;

import uz.pdp.instagram.controller.AuthUI;

public class Main {
    public static void main(String[] args) {
        AuthUI authUI = new AuthUI();
        authUI.start();
    }
}