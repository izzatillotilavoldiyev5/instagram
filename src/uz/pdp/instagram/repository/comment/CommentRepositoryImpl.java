package uz.pdp.instagram.repository.comment;

import uz.pdp.instagram.domain.model.user.Comment;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class CommentRepositoryImpl implements CommentRepository{
    private final ArrayList<Comment> COMMENT_LIST = new ArrayList<>();
    private static final CommentRepositoryImpl instance = new CommentRepositoryImpl();
    private CommentRepositoryImpl() {
        
    }

    public static CommentRepositoryImpl getInstance() {
        return instance;
    }
    @Override
    public void save(Comment comment) {

    }

    @Override
    public ArrayList<Comment> getAll() {
        return null;
    }

    @Override
    public void remove(UUID id) {

    }

    @Override
    public void remove(Comment comment) {

    }

    @Override
    public ArrayList<Comment> userComments(UUID postId) {
        ArrayList<Comment> userComments = new ArrayList<>();
        for (Comment comment : COMMENT_LIST) {
            if (Objects.equals(comment.getPostId(), postId)) {
                userComments.add(comment);
            }
        }
        return userComments;
    }
}
