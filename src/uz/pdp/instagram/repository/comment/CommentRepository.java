package uz.pdp.instagram.repository.comment;

import uz.pdp.instagram.domain.model.user.Comment;
import uz.pdp.instagram.repository.BaseRepository;

import java.util.ArrayList;
import java.util.UUID;

public interface CommentRepository extends BaseRepository<Comment> {
    ArrayList<Comment> userComments(UUID postId);
}
