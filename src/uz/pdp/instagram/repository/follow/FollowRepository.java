package uz.pdp.instagram.repository.follow;

import uz.pdp.instagram.domain.model.user.Follow;
import uz.pdp.instagram.domain.model.user.UserStatus;
import uz.pdp.instagram.repository.BaseRepository;

import java.util.ArrayList;
import java.util.UUID;

public interface FollowRepository extends BaseRepository<Follow> {
    ArrayList<UUID> getFollowIds(UUID userId, UserStatus userStatus);
}
