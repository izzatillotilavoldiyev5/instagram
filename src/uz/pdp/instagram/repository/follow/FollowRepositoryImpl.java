package uz.pdp.instagram.repository.follow;

import uz.pdp.instagram.domain.model.user.Follow;
import uz.pdp.instagram.domain.model.user.UserStatus;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class FollowRepositoryImpl implements FollowRepository{
    private final ArrayList<Follow> FOLLOW_LIST = new ArrayList<>();
    private static final FollowRepositoryImpl instance = new FollowRepositoryImpl();

    public FollowRepositoryImpl() {
    }

    public static FollowRepositoryImpl getInstance() {
        return instance;
    }

    @Override
    public void save(Follow follow) {
        FOLLOW_LIST.add(follow);
    }

    @Override
    public ArrayList<Follow> getAll() {
        return FOLLOW_LIST;
    }

    @Override
    public void remove(UUID id) {
        for (Follow follow : FOLLOW_LIST) {
            if (Objects.equals(follow.getId(), id)) {
                FOLLOW_LIST.remove(follow);
                return;
            }
        }
    }

    @Override
    public void remove(Follow follow) {
        for (Follow follow1 : FOLLOW_LIST) {
            if (Objects.equals(follow1.getFollowingId(), follow.getFollowingId())
                    && Objects.equals(follow1.getFollowerId(), follow.getFollowerId())) {
                FOLLOW_LIST.remove(follow1);
                return;
            }
        }
    }


    @Override
    public ArrayList<UUID> getFollowIds(UUID userId, UserStatus userStatus) {
        ArrayList<UUID> userFollows = new ArrayList<>();
        if (Objects.equals(userStatus, UserStatus.FOLLOWER)) {
            for (Follow follow : FOLLOW_LIST) {
                if (Objects.equals(follow.getFollowerId(), userId)) {
                    userFollows.add(follow.getFollowingId());
                }
            }
        }else {
            for (Follow follow : FOLLOW_LIST) {
                if (Objects.equals(follow.getFollowingId(), userId)) {
                    userFollows.add(follow.getFollowerId());
                }
            }
        }
        return userFollows;
    }
}
