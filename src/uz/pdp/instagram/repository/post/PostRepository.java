package uz.pdp.instagram.repository.post;

import uz.pdp.instagram.domain.model.user.Post;
import uz.pdp.instagram.repository.BaseRepository;

import java.util.ArrayList;
import java.util.UUID;

public interface PostRepository extends BaseRepository<Post> {
    ArrayList<Post> userPosts(UUID userId);

}
