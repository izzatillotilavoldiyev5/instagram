package uz.pdp.instagram.repository.post;

import uz.pdp.instagram.domain.model.user.Post;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class PostRepositoryImpl implements PostRepository{
    private final ArrayList<Post> POST_LISTS = new ArrayList<>();
    private static final PostRepositoryImpl instance = new PostRepositoryImpl();

    private PostRepositoryImpl() {
    }

    public static PostRepositoryImpl getInstance() {
        return instance;
    }
    @Override
    public void save(Post post) {
        POST_LISTS.add(post);
    }

    @Override
    public ArrayList<Post> getAll() {
        return null;
    }

    @Override
    public void remove(UUID id) {

    }

    @Override
    public void remove(Post post) {

    }

    @Override
    public ArrayList<Post> userPosts(UUID userId) {
        ArrayList<Post> userPosts = new ArrayList<>();
        for (Post post : POST_LISTS) {
            if (Objects.equals(post.getId(), userId)) {
                userPosts.add(post);
            }
        }
        return userPosts;
    }

}
