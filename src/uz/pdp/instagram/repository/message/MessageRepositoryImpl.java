package uz.pdp.instagram.repository.message;

import uz.pdp.instagram.domain.model.user.Message;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class MessageRepositoryImpl implements MessageRepository{

    private final ArrayList<Message> MESSAGE_LIST = new ArrayList<>();
    private static final MessageRepositoryImpl instance = new MessageRepositoryImpl();
    private MessageRepositoryImpl() {

    }

    public static MessageRepositoryImpl getInstance() {
        return instance;
    }
    @Override
    public void save(Message message) {
        MESSAGE_LIST.add(message);
    }

    @Override
    public ArrayList<Message> getAll() {
        return MESSAGE_LIST;
    }

    @Override
    public void remove(UUID id) {

    }

    @Override
    public void remove(Message message) {

    }

    @Override
    public ArrayList<Message> userMessages(UUID senderId, UUID receiverId) {
        ArrayList<Message> userMessages = new ArrayList<>();
        for (Message message : MESSAGE_LIST) {
            if (Objects.equals(message.getSenderId(), senderId)
                    && Objects.equals(message.getReceiverId(), receiverId)) {
                userMessages.add(message);
            }else if (Objects.equals(message.getSenderId(), receiverId)
                    && Objects.equals(message.getReceiverId(), senderId)) {
                userMessages.add(message);
            }
        }
        return userMessages;
    }

    @Override
    public ArrayList<UUID> chatIds(UUID userId) {
        ArrayList<UUID> chatIds = new ArrayList<>();
        for (Message message : MESSAGE_LIST) {
            if (Objects.equals(message.getSenderId(), userId)) {
                if (!chatIds.contains(message.getReceiverId())) {
                    chatIds.add(message.getReceiverId());
                }
            }else if (Objects.equals(message.getReceiverId(), userId)){
                if (!chatIds.contains(message.getSenderId())) {
                    chatIds.add(message.getSenderId());
                }
            }
        }
        return chatIds;
    }


}
