package uz.pdp.instagram.repository.message;

import uz.pdp.instagram.domain.model.user.Message;
import uz.pdp.instagram.repository.BaseRepository;

import java.util.ArrayList;
import java.util.UUID;

public interface MessageRepository extends BaseRepository<Message> {
    ArrayList<Message> userMessages(UUID senderId, UUID receiverId);
    ArrayList<UUID> chatIds(UUID userId);
}
