package uz.pdp.instagram.repository.user;

import uz.pdp.instagram.domain.model.user.User;

import java.util.ArrayList;
import java.util.UUID;

public class UserRepositoryImpl implements UserRepository {
    private final ArrayList<User> USER_LIST = new ArrayList<>();

    private static final UserRepositoryImpl instance = new UserRepositoryImpl();

    private UserRepositoryImpl() {
    }

    public static UserRepositoryImpl getInstance() {
        return instance;
    }

    @Override
    public void save(User user) {
        USER_LIST.add(user);
    }

    @Override
    public ArrayList<User> getAll() {
        return USER_LIST;
    }

    @Override
    public void remove(UUID id) {

    }

    @Override
    public void remove(User user) {

    }

}
