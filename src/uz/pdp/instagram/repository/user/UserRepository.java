package uz.pdp.instagram.repository.user;

import uz.pdp.instagram.domain.model.user.User;
import uz.pdp.instagram.repository.BaseRepository;

public interface UserRepository extends BaseRepository<User> {
}
