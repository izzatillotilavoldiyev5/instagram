package uz.pdp.instagram.repository;

import uz.pdp.instagram.domain.model.BaseModel;

import java.util.ArrayList;
import java.util.UUID;

public interface BaseRepository<T extends BaseModel> {
    void save(T t);

    ArrayList<T> getAll();
    void remove(UUID id);
    void remove(T t);
}
