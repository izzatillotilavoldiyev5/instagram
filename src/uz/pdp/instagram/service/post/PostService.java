package uz.pdp.instagram.service.post;

import uz.pdp.instagram.domain.DTO.Response;
import uz.pdp.instagram.domain.model.user.Comment;
import uz.pdp.instagram.domain.model.user.Post;
import uz.pdp.instagram.service.BaseService;

import java.util.ArrayList;
import java.util.UUID;

public interface PostService extends BaseService<Post> {
    ArrayList<Post> userPosts(UUID userId);
    Response addComment(Comment comment);
    ArrayList<Comment> userComments(UUID postId);
}
