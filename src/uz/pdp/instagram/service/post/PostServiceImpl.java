package uz.pdp.instagram.service.post;

import uz.pdp.instagram.domain.DTO.Response;
import uz.pdp.instagram.domain.model.user.Comment;
import uz.pdp.instagram.domain.model.user.Post;
import uz.pdp.instagram.repository.comment.CommentRepository;
import uz.pdp.instagram.repository.comment.CommentRepositoryImpl;
import uz.pdp.instagram.repository.post.PostRepository;
import uz.pdp.instagram.repository.post.PostRepositoryImpl;

import java.util.ArrayList;
import java.util.UUID;

public class PostServiceImpl implements PostService{
    private final PostRepository postRepository = PostRepositoryImpl.getInstance();
    private final CommentRepository commentRepository = CommentRepositoryImpl.getInstance();
    @Override
    public Response add(Post post) {
        postRepository.save(post);
        return new Response("Success", 200);
    }

    @Override
    public ArrayList<Post> userPosts(UUID userId) {
        ArrayList<Post> userPosts = postRepository.userPosts(userId);
        return userPosts;
    }

    @Override
    public Response addComment(Comment comment) {
        commentRepository.save(comment);
        return new Response<>("Success", 200);
    }

    @Override
    public ArrayList<Comment> userComments(UUID postId) {
        return commentRepository.userComments(postId);

    }
}
