package uz.pdp.instagram.service.user;

import uz.pdp.instagram.domain.DTO.Response;
import uz.pdp.instagram.domain.DTO.SignUpDTO;
import uz.pdp.instagram.domain.model.user.User;
import uz.pdp.instagram.service.BaseService;

import java.util.ArrayList;
import java.util.UUID;

public interface UserService extends BaseService<SignUpDTO> {
    Response signIn(String phoneNumber, String password);
    User searchUser(String name);
    ArrayList<User> userChats(ArrayList<UUID> chatIds);
}
