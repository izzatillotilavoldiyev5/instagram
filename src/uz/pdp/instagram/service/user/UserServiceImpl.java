package uz.pdp.instagram.service.user;

import uz.pdp.instagram.domain.DTO.Response;
import uz.pdp.instagram.domain.DTO.SignUpDTO;
import uz.pdp.instagram.domain.model.user.User;
import uz.pdp.instagram.exception.MyCustomException;
import uz.pdp.instagram.repository.user.UserRepository;
import uz.pdp.instagram.repository.user.UserRepositoryImpl;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class UserServiceImpl implements UserService{
    private final UserRepository userRepository = UserRepositoryImpl.getInstance();

    @Override
    public Response add(SignUpDTO userDTO) {
        if (doesUserExists(userDTO.phoneNumber())) {
            return new Response("This user already exists", 400);
        }

        User user = new User.UserBuilder()
                .setName(userDTO.name())
                .setPhoneNumber(userDTO.phoneNumber())
                .setPassword(userDTO.password())
                .build();
        userRepository.save(user);
        return new Response("Success", 200);
    }

    private boolean doesUserExists(String phoneNumber) {
        for (User user : userRepository.getAll()) {
            if (Objects.equals(user.getPhoneNumber(), phoneNumber)) {
                return true;
            }
        }
        return false;
    }

    @Override
    public Response signIn(String phoneNumber, String password) {

        for (User user : userRepository.getAll()) {
            if (Objects.equals(user.getPhoneNumber(), phoneNumber)
                    && Objects.equals(user.getPassword(), password)) {
                return new Response<>("Success", user, 200);
            }
        }
        throw new MyCustomException("User not found exception");
    }

    @Override
    public User searchUser(String name) {
        for (User user : userRepository.getAll()) {
            if(user.getName().toLowerCase().startsWith(name.toLowerCase())) {
                return user;
            }
        }
        return null;
    }

    @Override
    public ArrayList<User> userChats(ArrayList<UUID> chatIds) {
        ArrayList<User> userChats = new ArrayList<>();
        for (User user : userRepository.getAll()) {
            if (chatIds.contains(user.getId())) {
                userChats.add(user);
            }
        }
        return userChats;
    }
}
