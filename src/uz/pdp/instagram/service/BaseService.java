package uz.pdp.instagram.service;

import uz.pdp.instagram.domain.DTO.Response;

public interface BaseService<CD> {
    Response add(CD cd);
}
