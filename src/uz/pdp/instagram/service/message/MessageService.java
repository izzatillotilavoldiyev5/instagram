package uz.pdp.instagram.service.message;

import uz.pdp.instagram.domain.model.user.Message;
import uz.pdp.instagram.service.BaseService;

import java.util.ArrayList;
import java.util.UUID;

public interface MessageService extends BaseService<Message> {
    ArrayList<Message> userMessages(UUID senderId, UUID receiverId);
}
