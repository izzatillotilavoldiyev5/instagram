package uz.pdp.instagram.service.message;

import uz.pdp.instagram.domain.DTO.Response;
import uz.pdp.instagram.domain.model.user.Message;
import uz.pdp.instagram.repository.message.MessageRepositoryImpl;

import java.util.ArrayList;
import java.util.UUID;

public class MessageServiceImpl implements MessageService{

    MessageRepositoryImpl messageRepository = MessageRepositoryImpl.getInstance();

    @Override
    public Response add(Message message) {
        messageRepository.save(message);
        return null;
    }

    @Override
    public ArrayList<Message> userMessages(UUID senderId, UUID receiverId) {
        return messageRepository.userMessages(senderId, receiverId);
    }
}
