package uz.pdp.instagram.service.follow;

import uz.pdp.instagram.domain.DTO.Response;
import uz.pdp.instagram.domain.model.user.Follow;
import uz.pdp.instagram.domain.model.user.User;
import uz.pdp.instagram.domain.model.user.UserStatus;
import uz.pdp.instagram.repository.follow.FollowRepository;
import uz.pdp.instagram.repository.follow.FollowRepositoryImpl;
import uz.pdp.instagram.repository.user.UserRepository;
import uz.pdp.instagram.repository.user.UserRepositoryImpl;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

public class FollowServiceImpl implements FollowService {
    FollowRepository followRepository = FollowRepositoryImpl.getInstance();
    UserRepository userRepository = UserRepositoryImpl.getInstance();

    @Override
    public Response add(Follow follow) {
        if (doesFollowed(follow.getFollowingId(), follow.getFollowerId())) {
            return new Response<>("You already followed to this user", 400);
        }
        followRepository.save(follow);
        return new Response("Success", 200);
    }

    @Override
    public ArrayList<User> follows(UUID userId, UserStatus userStatus) {
        ArrayList<UUID> followIds = followRepository.getFollowIds(userId, userStatus);
        ArrayList<User> follows = new ArrayList<>();
        for (User user : userRepository.getAll()) {
            if (followIds.contains(user.getId())) {
                follows.add(user);
            }
        }
        return follows;
    }

    @Override
    public Response unFollow(UUID followingId, UUID followerId) {
        if (doesFollowed(followingId, followerId)) {
            Follow follow = new Follow(followingId, followerId);
            followRepository.remove(follow);
            return new Response<>("Success", 200);
        }
        return new Response<>("You don't followed to this user yet", 400);
    }

    private boolean doesFollowed(UUID followingId, UUID followerId) {
        for (Follow follow : followRepository.getAll()) {
            if (Objects.equals(follow.getFollowingId(), followingId)
                    && Objects.equals(follow.getFollowerId(), followerId)) {
                return true;
            }
        }
        return false;
    }

}
