package uz.pdp.instagram.service.follow;

import uz.pdp.instagram.domain.DTO.Response;
import uz.pdp.instagram.domain.model.user.Follow;
import uz.pdp.instagram.domain.model.user.User;
import uz.pdp.instagram.domain.model.user.UserStatus;
import uz.pdp.instagram.service.BaseService;

import java.util.ArrayList;
import java.util.UUID;

public interface FollowService extends BaseService<Follow> {
    ArrayList<User> follows(UUID userId, UserStatus userStatus);
    Response unFollow(UUID followingId, UUID followerId);
}
