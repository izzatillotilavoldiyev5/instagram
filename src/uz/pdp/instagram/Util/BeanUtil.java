package uz.pdp.instagram.Util;

import uz.pdp.instagram.controller.UserController;
import uz.pdp.instagram.repository.message.MessageRepositoryImpl;
import uz.pdp.instagram.service.follow.FollowService;
import uz.pdp.instagram.service.follow.FollowServiceImpl;
import uz.pdp.instagram.service.message.MessageService;
import uz.pdp.instagram.service.message.MessageServiceImpl;
import uz.pdp.instagram.service.post.PostService;
import uz.pdp.instagram.service.post.PostServiceImpl;
import uz.pdp.instagram.service.user.UserService;
import uz.pdp.instagram.service.user.UserServiceImpl;

import java.util.Scanner;

public interface BeanUtil {
    Scanner scanNum = new Scanner(System.in);
    Scanner scanStr = new Scanner(System.in);
    UserService userService = new UserServiceImpl();
    UserController userController = new UserController();
    FollowService followService = new FollowServiceImpl();
    PostService postService = new PostServiceImpl();
    MessageRepositoryImpl messageRepository = MessageRepositoryImpl.getInstance();
    MessageService messageService = new MessageServiceImpl();
}
