package uz.pdp.instagram.domain.model.user;

import uz.pdp.instagram.domain.model.BaseModel;

import java.util.UUID;

public class Comment extends BaseModel {
    private String text;
    private UUID postId;
    private String byWho;

    public Comment(String text, UUID postId, String byWho) {
        this.text = text;
        this.postId = postId;
        this.byWho = byWho;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public UUID getUserId() {
        return postId;
    }

    public void setUserId(UUID postId) {
        this.postId = postId;
    }

    public UUID getPostId() {
        return postId;
    }

    public void setPostId(UUID postId) {
        this.postId = postId;
    }

    public String getByWho() {
        return byWho;
    }

    public void setByWho(String byWho) {
        this.byWho = byWho;
    }

    @Override
    public String toString() {
        return byWho + ":  " + text;
    }
}
