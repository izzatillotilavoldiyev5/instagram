package uz.pdp.instagram.domain.model.user;

import uz.pdp.instagram.domain.model.BaseModel;

import java.util.UUID;

public class Follow extends BaseModel {
    private UUID followerId;
    private UUID followingId;

    public Follow(UUID following, UUID followerId) {
        this.followingId = following;
        this.followerId = followerId;
    }

    public Follow() {
    }

    public UUID getFollowerId() {
        return followerId;
    }

    public void setFollowerId(UUID followerId) {
        this.followerId = followerId;
    }

    public UUID getFollowingId() {
        return followingId;
    }

    public void setFollowingId(UUID followingId) {
        this.followingId = followingId;
    }
}
