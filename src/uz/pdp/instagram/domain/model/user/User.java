package uz.pdp.instagram.domain.model.user;

import uz.pdp.instagram.domain.model.BaseModel;

public class User extends BaseModel {
    private String name;
    private String phoneNumber;
    private String password;
    private UserStatus role;

    public User(UserBuilder userBuilder) {
        this.name = userBuilder.name;
        this.phoneNumber = userBuilder.phoneNumber;
        this.password = userBuilder.password;
        this.role = userBuilder.role;
    }

    public static class UserBuilder {
        private String name;
        private String phoneNumber;
        private String password;
        private UserStatus role;

        public User build() {
            return new User(this);
        }

        public UserBuilder setName(String name) {
            this.name = name;
            return this;
        }

        public UserBuilder setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
            return this;
        }

        public UserBuilder setPassword(String password) {
            this.password = password;
            return this;
        }

        public UserBuilder setRole(UserStatus role) {
            this.role = role;
            return this;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserStatus getRole() {
        return role;
    }

    public void setRole(UserStatus role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return name;
    }
}
