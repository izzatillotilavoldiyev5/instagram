package uz.pdp.instagram.domain.model.user;

import uz.pdp.instagram.domain.model.BaseModel;

public class Post extends BaseModel {
    private String title;
    private String text;
    private int like;

    public Post(String title, String text) {
        this.title = title;
        this.text = text;
    }

    public Post() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getLike() {
        return like;
    }

    public void setLike(int like) {
        this.like = like;
    }

    @Override
    public String toString() {
        return title;
    }
}
