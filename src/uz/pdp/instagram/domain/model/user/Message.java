package uz.pdp.instagram.domain.model.user;

import uz.pdp.instagram.domain.model.BaseModel;

import java.util.UUID;

public class Message extends BaseModel {
    private String message;
    private UUID senderId;
    private UUID receiverId;

    public Message(String message, UUID senderId, UUID receiverId) {
        this.message = message;
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

    public Message() {
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UUID getSenderId() {
        return senderId;
    }

    public void setSenderId(UUID senderId) {
        this.senderId = senderId;
    }

    public UUID getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(UUID receiverId) {
        this.receiverId = receiverId;
    }

    @Override
    public String toString() {
        return message;
    }
}
