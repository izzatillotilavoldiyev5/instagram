package uz.pdp.instagram.domain.model.user;

public enum UserStatus {
    FOLLOWER,
    FOLLOWING
}
