package uz.pdp.instagram.domain.model;

import java.util.UUID;

public abstract class BaseModel {
    {
        this.id = UUID.randomUUID();
    }
    private UUID id;

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }
}
