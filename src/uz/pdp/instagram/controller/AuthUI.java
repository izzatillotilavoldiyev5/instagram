package uz.pdp.instagram.controller;

import uz.pdp.instagram.domain.DTO.Response;
import uz.pdp.instagram.domain.DTO.SignUpDTO;
import uz.pdp.instagram.domain.model.user.User;

import static uz.pdp.instagram.Util.BeanUtil.*;


public class AuthUI {
    public void start() {
        int action;
        while (true) {
            System.out.println("1. Sign In\t  2. Sign Up\t  0. Exit");
            action = scanNum.nextInt();
            switch (action) {
                case 1 ->
                        signIn();

                case 2 ->
                        signUp();

                case 0 -> {
                    System.out.println("Thank you!"); return;
                }
            }
        }
    }

    private void signIn() {
        System.out.print("Phone Number: ");
        String phoneNumber = scanStr.nextLine();

        System.out.print("Password: ");
        String password = scanStr.nextLine();

        Response response = userService.signIn(phoneNumber, password);
        if (response.getData() == null) System.out.println(response.getMessage());
        else userController.userMenu((User) response.getData());
    }

    private void signUp() {
        System.out.print("Name: ");
        String name = scanStr.nextLine();

        System.out.print("Phone Number: ");
        String phoneNumber = scanStr.nextLine();

        System.out.print("Password: ");
        String password = scanStr.nextLine();

        SignUpDTO userDTO = new SignUpDTO(name, phoneNumber, password);

        Response response = userService.add(userDTO);
        System.out.println(response);
    }

    static  {
        SignUpDTO signUpDTO1 = new SignUpDTO("1", "1", "1");
        SignUpDTO signUpDTO2 = new SignUpDTO("2", "2", "2");
        SignUpDTO signUpDTO3 = new SignUpDTO("3", "3", "3");
        userService.add(signUpDTO1);
        userService.add(signUpDTO2);
        userService.add(signUpDTO3);
    }
}
