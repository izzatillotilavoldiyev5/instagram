package uz.pdp.instagram.controller;

import uz.pdp.instagram.domain.DTO.Response;
import uz.pdp.instagram.domain.model.user.*;

import java.util.ArrayList;
import java.util.Objects;
import java.util.UUID;

import static uz.pdp.instagram.Util.BeanUtil.*;

public class UserController {

    public void userMenu(User user) {
        while (true) {
            int action;
            System.out.println("1. Follow     2. Message     3. Posts     0. Exit");
            action = scanNum.nextInt();
            switch (action) {
                case 1 -> follow(user);
                case 2 -> message(user);
                case 3 -> OwnPosts(user);
                case 0 -> {
                    System.out.println("Thank you!");
                    return;
                }
            }
        }
    }

    private void follow(User user) {
        while (true) {
            System.out.println("1. Followers     2. Following     3. Search     0. Back");
            int action = scanNum.nextInt();
            switch (action) {
                case 1 -> userFollows(user, UserStatus.FOLLOWER);
                case 2 -> userFollows(user, UserStatus.FOLLOWING);
                case 3 -> {
                    User cUser = search();
                    if (cUser == null) {
                        System.out.println("Not found");
                        return;
                    }else {
                        userBody(user, cUser);
                    }
                }
                case 0 -> {
                    System.out.println("Thank you!");
                    return;
                }
            }
        }
    }

    private void userFollows(User user, UserStatus userStatus) {
        ArrayList<User> follows = followService.follows(user.getId(), userStatus);
        int i = 1;
        for (User user1 : follows) {
            System.out.println(i++ + ". " + user1);
        }
        System.out.println();

        try {
            System.out.print("choose (0. Back): ");
            int chosen = scanNum.nextInt();
            if (chosen == 0) {
                return;
            }
            User cUser = follows.get(chosen - 1);
            userBody(user, cUser);
        } catch (IndexOutOfBoundsException exception) {
            System.out.println("Wrong input... try again");
            scanNum.nextLine();
        }

    }

    private User search() {
        System.out.print("Search: ");
        String search = scanStr.nextLine();
        return userService.searchUser(search);
    }

    private void userBody(User user, User cUser) {
        while (true) {
            System.out.println(cUser + "\n");
            System.out.println("1. Follow     2. Un_Follow     3. See profile     0. Back");
            int action = scanNum.nextInt();
            switch (action) {
                case 1 -> {
                    Follow follow = new Follow(user.getId(), cUser.getId());
                    Response response = followService.add(follow);
                    System.out.println(response.getMessage());
                }
                case 2 -> {
                    System.out.println(followService.unFollow(user.getId(), cUser.getId()));
                    return;
                }

                case 3 -> userProfile(cUser);
                case 0 -> {
                    System.out.println("Thank you!");
                    return;
                }
            }
        }
    }

    private void message(User user) {
        while (true) {
            System.out.println("1. Chats     2. Search     0. Back");
            int action = scanNum.nextInt();
            switch (action) {
                case 1 -> chats(user);
                case 2 -> {
                    User cUser = search();
                    if (cUser == null) {
                        System.out.println("User not found");
                        return;
                    }else {
                        displayChat(user, cUser);
                    }
                }
                case 0 -> {
                    System.out.println("Thank you!");
                    return;
                }
            }
        }

    }

    private void chats(User user) {
        ArrayList<User> userChats = userService.userChats(messageRepository.chatIds(user.getId()));
        if (userChats == null) {
            System.out.println("You don't have any chats yet");
            return;
        }
        int i = 1;
        for (User chat : userChats) {
            System.out.println(i++ + ". " + chat);
        }
        try {
            System.out.println();
            System.out.print("choose: (0. Back)");
            int chosen = scanNum.nextInt();
            if (chosen == 0) {
                return;
            }
            User user1 = userChats.get(chosen - 1);
            displayChat(user, user1);
        } catch (IndexOutOfBoundsException exception) {
            System.out.println("Wrong input... try again");
            scanNum.nextLine();
        }
    }

    private void displayChat(User user, User cUser) {
        System.out.println("***************  " + cUser.getName() + "  ***************");
        ArrayList<Message> messages = messageService.userMessages(user.getId(), cUser.getId());
        for (Message message : messages) {
            if (Objects.equals(message.getReceiverId(), user.getId())) {
                System.out.println(message.getMessage());
            }else {
                System.out.println("\t\t\t" + message.getMessage());
            }
        }
        writeMessage(user.getId(), cUser.getId());
    }

    private void writeMessage(UUID senderId, UUID receiverId) {
        while (true) {
            System.out.print("message (0. back): ");
            String text = scanStr.nextLine();
            if (Objects.equals(text, "0")) {
                return;
            }
            Message message = new Message(text, senderId, receiverId);
            messageService.add(message);
        }
    }

    private void OwnPosts(User user) {
        while (true) {
            System.out.println("1. Creat post     2. My posts     0. Back");
            int action = scanNum.nextInt();
            switch (action) {
                case 1 -> createPost(user);

                case 2 -> myPosts(user);

                case 0 -> {
                    System.out.println("Thank you!");
                    return;
                }
            }
        }
    }

    private void createPost(User user) {
        System.out.print("Title: ");
        String title = scanStr.nextLine();

        System.out.print("Text: ");
        String text = scanStr.nextLine();

        Post post = new Post(title, text);
        post.setId(user.getId());
        Response response = postService.add(post);
        System.out.println(response);
    }

    private void myPosts(User user) {
        ArrayList<Post> userPosts = postService.userPosts(user.getId());
        int i = 1;
        for (Post post : userPosts) {
            System.out.println(i++ + ". " + post);
        }
        System.out.println();
        try {
            System.out.print("choose (0. Back): ");
            int chosen = scanNum.nextInt();
            if (chosen == 0) {
                return;
            }
            Post post = userPosts.get(chosen - 1);
            System.out.println("        " + post + "\n" + post.getText() + "\n" + "likes -> " + post.getLike());
            for (Comment comment : postService.userComments(post.getId())) {
                System.out.println(comment);
            }
        } catch (IndexOutOfBoundsException exception) {
            System.out.println("Wrong input... try again");
        }

    }

    private void userProfile(User user) {
        while (true) {
            System.out.println("1. Followers     2. Following     3. Posts     0. Back");
            int action = scanNum.nextInt();
            switch (action) {
                case 1 -> userFollows(user, UserStatus.FOLLOWER);
                case 2 -> userFollows(user, UserStatus.FOLLOWING);
                case 3 -> currentUserPosts(user);
                case 0 -> {
                    System.out.println("Thank you!");
                    return;
                }
            }
        }
    }

    private void currentUserPosts(User user) {
        ArrayList<Post> userPosts = postService.userPosts(user.getId());
        int i = 1;
        for (Post post : userPosts) {
            System.out.println(i++ + ". " + post);
        }
        System.out.println();
        System.out.print("choose: (0. Back)");
        int chosen = scanNum.nextInt();
        if (chosen != 0) {
            Post post = userPosts.get(chosen - 1);
            System.out.println(post);
            System.out.println("1. Like     2. Comment     0. Back");
            int action = scanNum.nextInt();
            switch (action) {
                case 1 -> {
                    post.setLike(post.getLike() + 1);
                    System.out.println("Liked" + '\n');
                }

                case 2 -> {
                    System.out.print("Comment: ");
                    String text = scanStr.nextLine();
                    Comment comment = new Comment(text, post.getId(), user.getName());
                    Response response = postService.addComment(comment);
                    System.out.println(response);
                }

                case 0 -> System.out.println("Thank you!");

            }
        }


    }


}
